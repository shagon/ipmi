FROM ubuntu:18.04

ENV DEBIAN_FRONTEND=noninteractive
ENV DISPLAY=:0.0

RUN apt-get update
RUN apt-get dist-upgrade -y --no-install-recommends
RUN apt-get install -y --no-install-recommends \
	fluxbox \
	firefox \
	git \
	software-properties-common \
	supervisor \
	x11vnc \
	xvfb \
	tar \
	wget
RUN git clone --branch v1.2.0 https://github.com/novnc/noVNC /opt/noVNC
RUN git clone https://github.com/novnc/websockify /opt/noVNC/utils/websockify
RUN wget https://www.supermicro.com/wdl/utility/IPMIView/Linux/IPMIView_2.21.0_build.221118_bundleJRE_Linux_x64.tar.gz && \
    tar zxvf IPMIView_2.20.0_build.220309_bundleJRE_Linux_x64.tar.gz && \
    mv IPMIView_2.20.0_build.220309_bundleJRE_Linux /opt/IPMIView && \
    rm IPMIView_2.20.0_build.220309_bundleJRE_Linux.tar.gz

RUN apt-get remove --purge -y git wget && \
	apt-get autoremove -y && \
	apt-get clean && \
	rm -rf /build && \
	rm -rf /tmp/* /var/tmp/* && \
	rm -rf /var/lib/apt/lists/*

ADD supervisord.conf /etc/supervisor/conf.d/supervisord.conf
ADD fluxbox-menu /root/.fluxbox/menu

EXPOSE 8080

CMD ["/usr/bin/supervisord"]
