## Docker container for Supermicro® IPMIView

This container runs:
  - Xvfb - X11 in a virtual framebuffer
  - x11vnc - A VNC server that scrapes the above X11 server
  - noNVC - A HTML5 canvas vnc viewer
  - Fluxbox - a small window manager
  - Firefox - web browser

## Usage

Build your own:

```
$ docker build -t shagon/ipmi:latest .
$ docker run -p 8080:8080 shagon/ipmi:latest
```

Then open your browser with address http://localhost:8080/vnc.html.